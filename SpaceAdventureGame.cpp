//
//  SpaceAdventureGame.cpp
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#include "SpaceAdventureGame.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>
#include "SpaceGameExceptions.h"

SpaceAdventureGame::SpaceAdventureGame() : gameShip(player) {}
void SpaceAdventureGame::load(){
    std::string temp;
    
    player->putInRoom(gameShip.getHelm());
    std::cout.setstate(std::ios_base::failbit); //suppresses output
    player->walkTo("AFT");
    player->walkTo("AFT");
    player->walkTo("STARBOARD");
    std::cout.clear(); //should unsuppress cout
    
    std::fstream file;
    file.open("save.txt");
    std::stringstream ss;
    
    std::cout.setstate(std::ios_base::failbit); //suppresses output
    
    if(file.peek()=='\a')phase0=true;
    currentRun.push_back("\a");
    
    file >> temp;
    while (file.peek()!=EOF&&file.peek()!='\a') {
        getUserInput(file, ss);
        getline(file,temp);
    }
    
    if(file.peek()=='\a'){
        phase2=true;
        currentRun.push_back("\a");
        currentRun.push_back("\a");
        file >> temp;
        file >> temp;
    }
    else if (file.peek()==EOF){
        file.close();
        return;
    }
    
    gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->unlock();
    gameShip.getHelm()->getAftRoom()->getAftRoom()->getPortRoom()->unlock();
    gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getStarboardRoom()->getPortRoom()->lock();
    gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getBowRoom()->getStarboardRoom()->lock();
    gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getPortRoom()->getBowRoom()->lock();
    
    
    while (file.peek()!=EOF&&file.peek()!='\a') {
        getUserInput(file, ss);
        getline(file,temp);
    }
    if(file.peek()=='\a'){
        phase6=true;
        currentRun.push_back("\a");
        file >> temp;
    }
    else if (file.peek()==EOF){
        file.close();
        return;
    }
    gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getBowRoom()->getStarboardRoom()->unlock();
    gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getPortRoom()->getBowRoom()->unlock();
    
    while (file.peek()!=EOF&&file.peek()!='\a') {
        getUserInput(file, ss);
        getline(file,temp);
    }
    if(file.peek()=='\a'){
        phase8=true;
        currentRun.push_back("\a");
        file >> temp;
    }
    else if (file.peek()==EOF){
        file.close();
        return;
    }
    
    file.close();
    std::cout.clear(); //should unsuppress cout
}

void SpaceAdventureGame::save(){
    std::fstream file;
    file.open("save.txt",std::fstream::out|std::fstream::trunc);
    file.close();
    file.open("./save.txt"/*,std::fstream::app*/);
    for (int i=0;i<currentRun.size();i++){
        file << currentRun[i] << std::endl;
        //std::cout << currentRun[i] <<"\n";
    }
    std::cout << "Game saved";
    file.close();
}

void SpaceAdventureGame::help(){
    std::cout << "-save\n-quit";
    Room* box = player->isIn;
    if (box->getBowRoom()!=nullptr) {
        std::cout << "\n-walk bow\n-inspect bow";
    }
    if (box->getAftRoom()!=nullptr) {
        std::cout << "\n-walk aft\n-inspect aft";
    }
    if (box->getPortRoom()!=nullptr) {
        std::cout << "\n-walk port\n-inspect port";
    }
    if (box->getStarboardRoom()!=nullptr) {
        std::cout << "\n-walk starboard\n-inspect starboard";
    }
    std::cout << "\n-inspect room";
    if (box->getNPC()!=nullptr) {
        std::cout << "\n-talk " << box->getNPC()->getName();
        std::cout << "\n-poke " << box->getNPC()->getName();
    }
    if (box->roomObjects.size()>=1) {
        for (int i=0; i<box->roomObjects.size(); i++) {
            std::cout << "\n-inspect " << box->roomObjects[i]->getName();
            std::cout << "\n-pickup " << box->roomObjects[i]->getName();
        }
    }
}

void SpaceAdventureGame::newGame(){
    player->putInRoom(gameShip.getHelm());
    std::cout.setstate(std::ios_base::failbit); //suppresses output
    player->walkTo("AFT");
    player->walkTo("AFT");
    player->walkTo("STARBOARD");
    std::cout.clear(); //should unsuppress cout
    std::fstream file;
    file.open("Dialog/1Opening");
    std::string out;
    getline(file,out);
    std::cout<<out<<"\n\n";
    file.close();
    file.open("save.txt",std::fstream::out|std::fstream::trunc);
    file.close();
}

void SpaceAdventureGame::dialog(std::string filename, int exit,int introLines){
    std::fstream file;
    file.open(filename);
    std::string out;
    std::cout<<"\n\n";
    for(;introLines;introLines--){
        getline(file,out);
        std::cout<<out<<"\n\n";
    }
    std::vector<std::string>options;
    std::vector<std::string>responces;
    while (file.peek()!=EOF) {
        getline(file,out);
        getline(file,out);
        options.push_back(out);
        getline(file,out);
        responces.push_back(out);
    }
    int in=0;
    do{
        for (int i=0; i<options.size(); i++) {
            std::cout << i <<". " << options[i] <<std::endl;
        }
        std::cout << ":";
        if (!(std::cin >>in)) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        else if(in>=0&&in<=exit) std::cout << "\n" << responces[in]<<"\n\n";
    }while (in!=exit);
    currentRun.push_back("\a");
    file.close();
}

void SpaceAdventureGame::play(){
    std::cout << "SPACE AN ADVENTURE\nOUT OF THIS WORLD\n";
    phase0=false;
    phase2=false;
    phase6=false;
    startGame();
    std::cout.clear(); //should unsuppress cout
    if (phase8==false) {
        if (phase6==false) {
            if (phase2==false) {
                if (phase0==false) {
                    
                    dialog("Dialog/2CaptainDialog1", 3,1);
                    
                }
                
                std::cout << player->inspect("ROOM");
                
                getUserInput(std::cin, std::cout);
                while (currentRun[currentRun.size()-1]!="QUIT"&&player->isIn!=gameShip.getHelm()) {
                    getUserInput(std::cin, std::cout);
                }
                if(currentRun[currentRun.size()-1]=="QUIT"){
                    return;
                }
                
                dialog("Dialog/3CaptainDialog2A", 3,1);
                
                dialog("Dialog/4CaptainDialog2B", 3,1);
                
                
                gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->unlock();
                gameShip.getHelm()->getAftRoom()->getAftRoom()->getPortRoom()->unlock();
                gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getStarboardRoom()->getPortRoom()->lock();
                gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getBowRoom()->getStarboardRoom()->lock();
                gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getPortRoom()->getBowRoom()->lock();
            }
            std::cout << player->inspect("ROOM");
            getUserInput(std::cin, std::cout);
            Room* engine=gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getBowRoom()->getAftRoom()->getBowRoom()->getBowRoom();
            bool nextstep=false;
            while (currentRun[currentRun.size()-1]!="QUIT"&&!nextstep) {
                getUserInput(std::cin, std::cout);
                if (player->isIn==engine) {
                    std::vector<Object*> inventory;
                    inventory=player->getInventory();
                    for (int i=0; i<inventory.size(); i++) {
                        if (inventory[i]->getName()=="Welding-Gun"){
                            for (int i=0; i<inventory.size(); i++) {
                                if (inventory[i]->getName()=="Thermionic-Valve"){
                                    for (int i=0; i<inventory.size(); i++) {
                                        if (inventory[i]->getName()=="Quantum-Entangler"){
                                            for (int i=0; i<inventory.size(); i++) {
                                                if (inventory[i]->getName()=="Plasma-Injector"){
                                                    for (int i=0; i<inventory.size(); i++) {
                                                        if (inventory[i]->getName()=="Cryogenic-Manifold"){
                                                            nextstep=true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(currentRun[currentRun.size()-1]=="QUIT"){
                return;
            }
            
            std::cout << "*Engine Online, Laundry Room unlocked* \n\n The vibrations of the engine starting must have unjammed the lock to the laundry room\n " ;//\n-saving dissabled from here on-";
            
            gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getBowRoom()->getStarboardRoom()->unlock();
            gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getPortRoom()->getBowRoom()->unlock();
            
        }
        
        bool nextstep=false;
        std::cout << player->inspect("ROOM");
        nextstep=false;
        getUserInput(std::cin, std::cout);
        Room* laundry =gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getPortRoom()->getBowRoom();
        while (currentRun[currentRun.size()-1]!="QUIT"&&!nextstep) {
            getUserInput(std::cin, std::cout);
            if (player->isIn==laundry) {
                nextstep=true;
            }
        }
        if(currentRun[currentRun.size()-1]=="QUIT"){
            return;
        }
        
        dialog("Dialog/5CaptainDialog3", 2,3);
        
    }
    std::cout << "\nYou hear a loud noise as the security system gets activated you should go to the defence room.\n\n";
    
    std::cout << player->inspect("ROOM");
    bool nextstep=false;
    getUserInput(std::cin, std::cout);
    Room* defence =gameShip.getHelm()->getAftRoom()->getAftRoom()->getAftRoom()->getAftRoom()->getPortRoom()->getBowRoom()->getStarboardRoom();
    while (currentRun[currentRun.size()-1]!="QUIT"&&!nextstep) {
        getUserInput(std::cin, std::cout);
        if (player->isIn==defence) {
            nextstep=true;
        }
    }
    if(currentRun[currentRun.size()-1]=="QUIT"){
        return;
    }
    bool knife = false;
    std::vector<Object*> inventory;
    inventory=player->getInventory();
    for (int i=0; i<inventory.size(); i++) {
        if (inventory[i]->getName()=="Sharp-Knife"){
            knife=true;
        }
    }
    std::cout << "\n";
    if (knife) {
        std::fstream file;
        file.open("Dialog/Security Dialog with knife");
        std::string out;
        getline(file,out);
        std::cout<<out<<"\n";
        getline(file,out);
        std::cout<<out<<"\n";
        std::vector<std::string>options;
        std::vector<std::string>responces;
        while (file.peek()!=EOF) {
            getline(file,out);
            getline(file,out);
            options.push_back(out);
            getline(file,out);
            responces.push_back(out);
        }
        int in=0;
        do{
            for (int i=0; i<options.size(); i++) {
                std::cout << i <<". " << options[i] <<std::endl;
            }
            std::cout << ":";
            if (!(std::cin >>in)) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            }
            else if(in>=0&&in<=3){
                std::cout << responces[in]<<"\n";
                if(in==3)throw file_formatting_error();
                if(in==2){
                    fstream file;
                    file.open("Dialog/Explosive Ending");
                    std::string temp;
                    getline(file,temp);
                    std::cout << temp;
                    return;
                }
                break;
            }
        }while (in!=3);
        file.close();
    }
    else{
        std::fstream file;
        file.open("Dialog/Security Dialog");
        std::string out;
        getline(file,out);
        std::cout<<out<<"\n";
        getline(file,out);
        std::cout<<out<<"\n";
        std::vector<std::string>options;
        std::vector<std::string>responces;
        while (file.peek()!=EOF) {
            getline(file,out);
            getline(file,out);
            options.push_back(out);
            getline(file,out);
            responces.push_back(out);
        }
        int in=0;
        do{
            for (int i=0; i<options.size(); i++) {
                std::cout << i <<". " << options[i] <<std::endl;
            }
            std::cout << ":";
            if (!(std::cin >>in)) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            }
            else if(in>=0&&in<=3){ std::cout << responces[in]<<"\n";
                if(in==2)throw file_formatting_error();
                if(in==1){
                    fstream file;
                    file.open("Dialog/Explosive Ending");
                    std::string temp;
                    getline(file,temp);
                    std::cout << temp;
                    return;
                }
                break;
            }
        }while (in!=3);
        file.close();
    }
    for (int i=0; i<inventory.size(); i++) {
        if (inventory[i]->getName()=="Gun"){
            
            fstream file;
            file.open("Dialog/gun and code ending");
            std::string temp;
            getline(file,temp);
            std::cout << temp;
            return;
            
        }
    }
}
