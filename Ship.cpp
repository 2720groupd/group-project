//
//  Ship.cpp
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#include "Ship.h"
#include "Room.h"
#include "Object.h"
#include "Pickup.h"

Room* Ship::getHelm(){
    return helm;
}

Ship::~Ship(){
    for (int i=0; i<allRooms.size(); i++) {
        delete allRooms[i];
    }
    for (int i=0; i<allNPCs.size(); i++) {
        delete allNPCs[i];
    }
    for (int i=0; i<allObjects.size(); i++) {
        delete allObjects[i];
    }
}

Ship::Ship(Player* mainChar){
    NPC*bob;
    Object*thing;
    //Captian Room
     bob=new NPC("Dialog/npc's/Captain","Dialog/npc's/CaptainRegular","Dialog/npc's/CaptainAnnoyed");
    allNPCs.push_back(bob);
    helm=new Room("Dialog/Rooms/Captains Helm","Captains Helm", nullptr, nullptr, nullptr, nullptr,bob);
    //Command Post
    bob=new NPC("Dialog/npc's/NPC1","Dialog/npc's/NPC1Regular","Dialog/npc's/NPC1Annoyed");
    allNPCs.push_back(bob);
    Room* nextRoom=new Room("Dialog/Rooms/Command Post","Command Post", helm, nullptr, nullptr, nullptr,bob);
    helm->setAftRoom(nextRoom);
    allRooms.push_back(helm);
    allRooms.push_back(nextRoom);
    
    //HALLWAY
    Room* prevRoom =nextRoom;
    nextRoom =new Room("Dialog/Rooms/Hallway","Hallway", prevRoom, nullptr, nullptr, nullptr,nullptr);
    prevRoom->setAftRoom(nextRoom);
    allRooms.push_back(nextRoom);

    //SENIOR CRYO ROOM
    Room* sideroom=new Room("Dialog/Rooms/Senior Staff Cryo Room","Senior Staff Cryo Room", nullptr, nullptr, nextRoom, nullptr,nullptr);
    thing=new Pickup("Dialog/Items/Quest Items/Cryogenic Manifold",false);
    sideroom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    
    nextRoom->setStarboardRoom(sideroom);
    allRooms.push_back(sideroom);
    
    //CAFETERIA
    bob=new NPC("Dialog/npc's/NPC5","Dialog/npc's/NPC5Regular","Dialog/npc's/NPC5Annoyed");
    allNPCs.push_back(bob);
    sideroom=new Room("Dialog/Rooms/Cafeteria","Cafeteria", nullptr, nullptr, nullptr, nextRoom,bob);
    sideroom->lock();
    nextRoom->setPortRoom(sideroom);
    allRooms.push_back(sideroom);
 
    thing=new Pickup("Dialog/Items/Pickup Items/Knife",false);
    sideroom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    
    thing=new Pickup("Dialog/Items/Pickup Items/Fish",false);
    sideroom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    
    
    //HALLWAY
    prevRoom =nextRoom;
    nextRoom =new Room("Dialog/Rooms/Hallway","Top Floor Hallway", prevRoom, nullptr, nullptr, nullptr,nullptr);
    nextRoom->lock();
    prevRoom->setAftRoom(nextRoom);
    allRooms.push_back(nextRoom);
    
    
    bob=new NPC("Dialog/npc's/NPC4","Dialog/npc's/NPC4Regular","Dialog/npc's/NPC4Annoyed");
    allNPCs.push_back(bob);
    sideroom=new Room("Dialog/Rooms/Exercise Room","Exercise Room", nullptr, nullptr, nextRoom, nullptr,bob);
    
    thing=new Pickup("Dialog/Items/Pickup Items/Dumbell",false);
    sideroom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    
    nextRoom->setStarboardRoom(sideroom);
    allRooms.push_back(sideroom);
    
    sideroom=new Room("Dialog/Rooms/Clinic","Clinic", nullptr, nullptr, nullptr, nextRoom,nullptr);
    
    thing=new Pickup("Dialog/Items/Quest Items/Plasma Injector",false);
    sideroom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    
    nextRoom->setPortRoom(sideroom);
    allRooms.push_back(sideroom);
    
    prevRoom =nextRoom;
    nextRoom =new Room("Dialog/Rooms/Elevator","Elevator", prevRoom, nullptr, nullptr, nullptr,nullptr);
    prevRoom->setAftRoom(nextRoom);
    allRooms.push_back(nextRoom);
    Room* elevator=nextRoom;
    
    
    nextRoom=new Room("Dialog/Rooms/Hallway","Bottom Floor Hallway",nullptr,elevator,nullptr,nullptr,nullptr);//bottom
    elevator->setStarboardRoom(nextRoom);
    allRooms.push_back(nextRoom);
    
    prevRoom =nextRoom;
    bob=new NPC("Dialog/npc's/NPC3","Dialog/npc's/NPC3Regular","Dialog/npc's/NPC3Annoyed");
    allNPCs.push_back(bob);
    nextRoom=new Room("Dialog/Rooms/Parts Room","Parts Room",nullptr,nullptr,prevRoom,nullptr,bob);
    
    thing=new Pickup("Dialog/Items/Quest Items/Welding Gun",false);
    nextRoom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    thing=new Pickup("Dialog/Items/Pickup Items/Power Supply",false);
    nextRoom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    thing=new Pickup("Dialog/Items/Pickup Items/Wrench",false);
    nextRoom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    
    prevRoom->setStarboardRoom(nextRoom);
    allRooms.push_back(nextRoom);
    
    nextRoom=new Room("Dialog/Rooms/Primary Cryo Room","Primary Cryo Room",nullptr,nullptr,nullptr,prevRoom,nullptr);
    prevRoom->setPortRoom(nextRoom);
    allRooms.push_back(nextRoom);
    
    nextRoom=new Room("Dialog/Rooms/Vegitation","Vegitation Room",nullptr,prevRoom,nullptr,nullptr,nullptr);//bottom
    nextRoom->roomObjects.push_back(new Pickup("Dialog/Items/Quest Items/Thermionic Valve",false));
    prevRoom->setBowRoom(nextRoom);
    allRooms.push_back(nextRoom);
    
    
    
    nextRoom=new Room("Dialog/Rooms/Hallway","Middle Floor Hallway",nullptr,elevator,nullptr,nullptr,nullptr);//middle
    elevator->setPortRoom(nextRoom);
    allRooms.push_back(nextRoom);
    
    prevRoom=nextRoom;
    bob=new NPC("Dialog/npc's/NPC2","Dialog/npc's/NPC2Regular","Dialog/npc's/NPC2Annoyed");
    allNPCs.push_back(bob);
    sideroom=new Room("Dialog/Rooms/Power Relay Room","Power Relay Room",nullptr,nullptr,prevRoom,nullptr,bob);
    
    thing=new Pickup("Dialog/Items/Quest Items/Quantum Entangler",false);
    sideroom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    thing=new Pickup("Dialog/Items/Pickup Items/Magnetic Gloves",false);
    sideroom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    
    prevRoom->setStarboardRoom(sideroom);
    allRooms.push_back(sideroom);
    
    Room* secondRoom=new Room("Dialog/Rooms/Utility Closet","Utility Closet",sideroom,nullptr,nullptr,nullptr,nullptr);
    sideroom->setAftRoom(secondRoom);
    allRooms.push_back(secondRoom);
    
    
    sideroom=new Room("Dialog/Rooms/Living Quarters","Living Quarters",nullptr,nullptr,nullptr,prevRoom,nullptr);
    prevRoom->setPortRoom(sideroom);
    allRooms.push_back(sideroom);
    
    thing=new Pickup("Dialog/Items/Quest Items/Gun",false);
    sideroom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    
    secondRoom=new Room("Dialog/Rooms/Laundry Room","Laundry Room",nullptr,sideroom,nullptr,nullptr,nullptr);
    sideroom->setBowRoom(secondRoom);
    allRooms.push_back(secondRoom);
    
    prevRoom=nextRoom;
    nextRoom=new Room("Dialog/Rooms/Hallway","Hallway",nullptr,prevRoom,nullptr,nullptr,nullptr);
    prevRoom->setBowRoom(nextRoom);
    allRooms.push_back(nextRoom);
    
    prevRoom=nextRoom;
    nextRoom=new Room("Dialog/Rooms/Defence System","Defence System",nullptr,nullptr,prevRoom,nullptr,nullptr);
    prevRoom->setStarboardRoom(nextRoom);
    allRooms.push_back(nextRoom);
    
    thing=new Pickup("Dialog/Items/Quest Items/Explosives",false);
    nextRoom->roomObjects.push_back(thing);
    allObjects.push_back(thing);
    
    
    nextRoom=new Room("Dialog/Rooms/Engine Room","Engine Room",nullptr,prevRoom,nullptr,nullptr,nullptr);
    prevRoom->setBowRoom(nextRoom);
    allRooms.push_back(nextRoom);
    
    
}
