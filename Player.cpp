//
//  Player.cpp
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#include "Player.h"
#include <algorithm>
#include "SpaceGameExceptions.h"
#include "Pickup.h"
#include "Object.h"
#include <typeinfo>

Player::Player(Room *start) : playerAlive(true){
    isIn=start;
}

std::string Player::inspect(std::string object){
    std::transform(object.begin(), object.end(),object.begin(), ::toupper);
    Object* item=nullptr;
    std::string temp,output;
    
    for (int i=0; i<playerInventory.size(); i++) {
        temp=playerInventory[i]->getName();
        std::transform(temp.begin(), temp.end(),temp.begin(), ::toupper);
        if (temp==object) {
            item=playerInventory[i];
        }
    }
    for (int i=0; i<isIn->roomObjects.size(); i++) {
        temp=isIn->roomObjects[i]->getName();
        std::transform(temp.begin(), temp.end(),temp.begin(), ::toupper);
        if (temp==object) {
            item=isIn->roomObjects[i];
        }
    }
    
    if (item!=NULL) {
        output=item->getDescription();
    }
    
    else if (item==NULL) {
        if (isIn->getPortRoom() != nullptr && object=="PORT" && isIn->getName()!="Elevator") {
            output=isIn->getPortRoom()->getName();
            if(isIn->getPortRoom()->islocked())
            {
                output+="*Locked*";
            }
        }
        else if (isIn->getStarboardRoom() != nullptr && object=="STARBOARD" && isIn->getName()!="Elevator") {
            output=isIn->getStarboardRoom()->getName();
            if(isIn->getStarboardRoom()->islocked())
            {
                output+="*Locked*";
            }
        }
        else if (isIn->getAftRoom() != nullptr && object=="AFT" && isIn->getName()!="Elevator") {
            output=isIn->getAftRoom()->getName();
            if(isIn->getAftRoom()->islocked())
            {
                output+="*Locked*";
            }
        }
        else if (isIn->getBowRoom() != nullptr && object=="BOW" && isIn->getName()!="Elevator") {
            output=isIn->getBowRoom()->getName();
            if(isIn->getBowRoom()->islocked())
            {
                output+="*Locked*";
            }
        }
        else if(isIn->getPortRoom() != nullptr && object=="MIDDLE" && isIn->getName()=="Elevator"){
            output=isIn->getPortRoom()->getName();
            if(isIn->getPortRoom()->islocked())
            {
                output+="*Locked*";
            }
        }
        else if (isIn->getStarboardRoom() != nullptr && object=="BOTTOM" && isIn->getName()=="Elevator"){
            output=isIn->getStarboardRoom()->getName();
            if(isIn->getStarboardRoom()->islocked())
            {
                output+="*Locked*";
            }
        }
        else if (isIn->getBowRoom() != nullptr && object=="TOP" && isIn->getName()=="Elevator"){
            output=isIn->getBowRoom()->getName();
            if(isIn->getBowRoom()->islocked())
            {
                output+="*Locked*";
            }
        }
        
        else if(object=="ROOM"){
            output=isIn->getDiscription();
            if(isIn->getNPC()!=nullptr){
                output+="\nNPC:";
                output+="\n- ";
                output+=isIn->getNPC()->getName();
            }
            if(isIn->roomObjects.size()!=0){
                bool anyVisible=false;
                for(int i = 0; (i<isIn->roomObjects.size() and !anyVisible); i++)
                {
                    if(!(isIn->roomObjects[i])->isHidden())
                    {
                        anyVisible=true;
                    }
                }
                if (anyVisible) {
                    output+="\n\nOBJECTS:";
                    for(int i = 0; i<isIn->roomObjects.size(); i++)
                    {
                        if(!(isIn->roomObjects[i])->isHidden())
                        {
                            output+="\n- ";
                            output+=isIn->roomObjects[i]->getName();
                        }
                    }
                }
            }
            
        }
        else if(object == "INVENTORY")
        {
            output= checkInventory();
        }
        else if(isIn->getNPC() != nullptr ){
            std::string NPCName =isIn->getNPC()->getName();
            std::transform(NPCName.begin(), NPCName.end(),NPCName.begin(), ::toupper);
            if( NPCName == object)
            {
                output = isIn->getNPC()->getDescription();
            }
        }
    }
    //This final else should only run if no proper word is entered, this will cause an exception to be thrown
    //in Game.cpp Everything else should go above this.
    else
    {
        return "";
    }
    
    
    return output;
    
    
}

void Player::walkTo(std::string whereTo){
    std::transform(whereTo.begin(), whereTo.end(),whereTo.begin(), ::toupper);
    if (isIn->getName()=="Elevator") {
        if (whereTo=="MIDDLE") {
            if(isIn->getPortRoom()==nullptr)
            {
                throw out_of_bounds_error();
            }
            if(!(isIn->getPortRoom()->islocked()))
            {
                isIn=isIn->getPortRoom();
            }
            else{
                std::cout << "That floor is locked.\n";
            }
        }
        else if (whereTo=="TOP") {
            if(isIn->getBowRoom()==nullptr)
            {
                throw out_of_bounds_error();
            }
            if(!(isIn->getBowRoom()->islocked()))
            {
                isIn=isIn->getBowRoom();
            }
            else{
                std::cout << "That floor is locked.\n";
            }
        }
        else if (whereTo=="BOTTOM") {
            if(isIn->getStarboardRoom()==nullptr)
            {
                throw out_of_bounds_error();
            }
            if(!(isIn->getStarboardRoom()->islocked()))
            {
                isIn=isIn->getStarboardRoom();
            }
            else{
                std::cout << "That floor is locked.\n";
            }
        }
        else
        {
            throw invalid_walk_input_error();
        }
        
    }
    else if(whereTo=="BOW"||whereTo=="AFT"||whereTo=="PORT"||whereTo=="STARBOARD"){
        if (whereTo=="BOW") {
            if(isIn->getBowRoom()==nullptr)
            {
                throw out_of_bounds_error();
            }
            if(!(isIn->getBowRoom()->islocked()))
            {
                isIn=isIn->getBowRoom();
            }
            else{
                std::cout << "That door is locked.\n";
            }
        }
        else if (whereTo=="AFT") {
            if(isIn->getAftRoom()==nullptr)
            {
                throw out_of_bounds_error();
            }
            if(!(isIn->getAftRoom()->islocked()))
            {
                isIn=isIn->getAftRoom();
            }
            else{
                std::cout << "That door is locked.\n";
            }
        }
        else if (whereTo=="PORT") {
            if(isIn->getPortRoom()==nullptr)
            {
                throw out_of_bounds_error();
            }
            if(!(isIn->getPortRoom()->islocked()))
            {
                isIn=isIn->getPortRoom();
            }
            else{
                std::cout << "That door is locked.\n";
            }
        }
        else if (whereTo=="STARBOARD") {
            if(isIn->getStarboardRoom()==nullptr)
            {
                throw out_of_bounds_error();
            }
            if(!(isIn->getStarboardRoom()->islocked()))
            {
                isIn=isIn->getStarboardRoom();
            }
            else{
                std::cout << "That door is locked.\n";
            }
        }
    }
    //This throws an error if any of the four directions are not chosen
    else
    {
        throw invalid_walk_input_error();
    }
    std::cout << inspect("ROOM");
    
}
void Player::putInRoom(Room* togoin){
    isIn=togoin;
}

void Player::killPlayer(){
    playerAlive=false;
}


std::string Player::checkInventory() const
{
    std::string inventoryTemp ="";
    if(playerInventory.size() == 0)
        return "Inventory is empty.";
    else
    {
        inventoryTemp+="Items currently held are:";
        for(int i=0; i<playerInventory.size(); i++)
        {
            inventoryTemp+="\n-";
            inventoryTemp+=playerInventory[i]->getName();
            
        }
        return inventoryTemp;
    }
}

std::string Player::talk(std::string charName) const
{
    std::transform(charName.begin(), charName.end(),charName.begin(), ::toupper);
    if(isIn->getNPC() != nullptr ){
        std::string NPCName =isIn->getNPC()->getName();
        std::transform(NPCName.begin(), NPCName.end(),NPCName.begin(), ::toupper);
        if( NPCName == charName)
        {
            return isIn->getNPC()->respond();
        }
    }
    return "";
}

std::string Player::poke(std::string charName) const
{
    std::transform(charName.begin(), charName.end(),charName.begin(), ::toupper);
    if(isIn->getNPC() != nullptr ){
        std::string NPCName =isIn->getNPC()->getName();
        std::transform(NPCName.begin(), NPCName.end(),NPCName.begin(), ::toupper);
        if( NPCName == charName)
        {
            return isIn->getNPC()->respondAnnoyed();
        }
    }
    return "";
}

vector<Object*> Player::getInventory()
{
    return playerInventory;
}

void Player::pickup(std::string itemName)
{
    std::transform(itemName.begin(), itemName.end(),itemName.begin(), ::toupper);
    if(isIn->getItem(itemName) == nullptr)
    {
        throw invalid_object_usage_error();
    }
    std::string temp=isIn->getItem(itemName)->getName();
    std::transform(temp.begin(), temp.end(),temp.begin(), ::toupper);
    
    if(temp == itemName && !(isIn->getItem(itemName)->isHidden()))
    {
        playerInventory.push_back(isIn->getItem(itemName));
        
        //removal of pointer from vector in room
        for(int i =0; i<isIn->roomObjects.size(); i++)
        {
            std::string temp = isIn->roomObjects[i]->getName();
            std::transform(temp.begin(), temp.end(),temp.begin(), ::toupper);
            if(temp == itemName)
            {
                isIn->roomObjects.erase(isIn->roomObjects.begin()+i);
            }
        }
    }
    else throw invalid_object_usage_error();
}