//**********************************************************
//  Pickup.cpp
//  Group d Project
//  File contains implementation for Pickup class, concrete class of Object
//
//  
//**********************************************************
#include "Pickup.h"

Pickup:: Pickup(std::string infoFile, bool hidden) : Object(infoFile,hidden), isUsed(false) {};
Pickup::~Pickup(){};

bool Pickup::getUsedStatus()
{
  return isUsed;
}

void Pickup::setUsed()
{
  isUsed = true;
}
