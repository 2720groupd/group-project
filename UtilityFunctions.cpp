
#include "UtilityFunctions.h"
#include "SpaceGameExceptions.h"
#include<iostream>
#include<fstream>
namespace utility
{
    std::vector<string> readInFileString(std::string fileName)
    {
        std::ifstream inFile;
        std::string line;
        std::vector<string> returnString;
        ///Method throws exception if a file is could not be opened and returns a vector with one string, a warning message.
        try
        {
            inFile.open(fileName);
            
            if(inFile.is_open())
            {
                while(std::getline(inFile, line))
                {
                    returnString.push_back(line);
                }
                inFile.close();
                return returnString;
            }
            else
            {
                throw file_unopened_error();
            }
        }
        catch(file_unopened_error& p)
        {
            ///With this error the game wil still work but whatever is meant to be displayed will not. If this is openeing a character info file a different error is also thrown.
            std::cout<<"Error: "<<p.what()<<" "<<fileName<<" warning string will be returned. It is advised that the program be closed to fix bad file." <<std::endl;
            returnString.push_back("This text file could not be properly loaded.");
            return returnString;
        }
    }
    
}
