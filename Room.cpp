//
//  Room.cpp
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#include "Room.h"
#include <fstream>
#include <algorithm>

Room::Room(std::string text,std::string roomName,Room* front,Room* back,Room* left,Room* right,NPC* npc){
    roomLocked=false;
    roomCharacter=npc;
    name=roomName;
    std::fstream container;
    container.open(text);
    getline(container,Discription);
    container.close();
    bow=front;
    aft=back;
    port=left;
    starboard=right;
}

Room::~Room(){

}

void Room::lock(){
    roomLocked=true;
}

void Room::unlock(){
    roomLocked=false;
}
void Room::setBowRoom(Room* front){
    bow = front;
}
void Room::setAftRoom(Room* back){
    aft = back;
}
void Room::setPortRoom(Room* left){
    port = left;
}
void Room::setStarboardRoom(Room* right){
    starboard = right;
}
Room* Room::getBowRoom(){
    return bow;
}
Room* Room::getAftRoom(){
    return aft;
}
Room* Room::getPortRoom(){
    return port;
}
Room* Room::getStarboardRoom(){
    return starboard;
}

bool Room::islocked(){
    return roomLocked;
}
std::string Room::getName(){
    return name;
}

std::string Room::getDiscription(){
    return Discription;
}

void Room::setNPC(NPC* bob){
    roomCharacter = bob;
}


NPC* Room::getNPC()
{
    return roomCharacter;
}

Object* Room::getItem(std::string itemName)
{
    std::transform(itemName.begin(), itemName.end(), itemName.begin(), ::toupper); //guarantees uppercase comparison
    for(int i = 0; i<roomObjects.size(); i++)
    {
        std::string upperTemp = roomObjects[i]->getName();
         std::transform(upperTemp.begin(), upperTemp.end(),upperTemp.begin(), ::toupper);
        if(upperTemp == itemName)
        {
            return roomObjects[i];
        }
    }
    return nullptr;
}