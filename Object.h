//**********************************************************
//  Object.h
//  Group d Project
//  File contains definitions for Object abstract class
//
//  
//**********************************************************

#ifndef Object_h
#define Object_h

#include<vector>
#include<iostream>
#include<string>
#include<fstream>


    /**
    *base class for all objects in the game
    */
class Object
{
public:
    /**
    *reads in the object description from their text file
    */
    Object(std::string InfoFile, bool hidden);
    /**
    *returns name of the object
    */
   virtual ~Object();
    std::string getName() const;
    /**
    *gets the description of the current item
    */
    std::string getDescription() const;
    /**
    *set name of the item
    */
    void setName(std::string name);
    /**
    *set description of the item
    */
    void setDescription(std::string description);
    /**
    *true or false whether the item is visible by the player
    */
    bool isHidden();
      /**
    *reveals the item to the player if something happens
    */
    void revealObject();

private:
    std::string objectName;
    std::string objectDescription;
    bool hidden;
};


#endif /* Object_h */
