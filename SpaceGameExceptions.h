//**********************************
//SpaceGameExceptions.h
//Project Group D
//**********************************

#ifndef SpaceGameExceptions_h
#define SpaceGameExceptions_h

#include<stdexcept>
using namespace std;

class invalid_walk_input_error : public runtime_error
{
public:
    invalid_walk_input_error(): runtime_error("Input direction invalid"){};
};

class invalid_command_error : public runtime_error
{
public:
    invalid_command_error(): runtime_error("Unknown Command"){};
};

class out_of_bounds_error : public runtime_error
{
public:
    out_of_bounds_error() : runtime_error("No valid room to enter"){}
};


class invalid_object_usage_error : public runtime_error
{
public:
    invalid_object_usage_error() : runtime_error("Invalid object usage"){}
};

class file_unopened_error :public runtime_error
{
public:
    file_unopened_error() : runtime_error("Could not open file named"){}
};

class file_formatting_error :public runtime_error
{
public:
    file_formatting_error() : runtime_error("Bad file formatting"){}
};

class invalid_inspection_parameter : public runtime_error
{
public:
    invalid_inspection_parameter() : runtime_error("Invalid inspection parameter"){}
};

class invalid_talk_error : public runtime_error
{
public:
    invalid_talk_error() : runtime_error("Invalid talk parameter"){}
};

class invalid_poke_error : public runtime_error
{
public:
    invalid_poke_error() : runtime_error("Invalid poke parameter"){}
};

#endif
