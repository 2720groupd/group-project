#include <iostream>
#include "Game.h"
#include "SpaceAdventureGame.h"
#include "Room.h"
#include "Player.h"
#include "SpaceGameExceptions.h"

int main(){
    //std::cout << "hello world\n";
    SpaceAdventureGame space;
    try {
        space.play();
    } catch (file_formatting_error) {
        std::cout << "GAME OVER YOU DIED";
    }

    //Room firstroom("discription1", nullptr, nullptr, nullptr, nullptr);
    //Room secondroom("discription2", &firstroom, nullptr, nullptr, nullptr);
    //firstroom.setAftRoom(&secondroom);
    //Player person(&firstroom);
    //person.walkTo("AFT");
    
    return 0;
}
