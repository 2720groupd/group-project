//
//  Game.cpp
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#include "Game.h"
#include "SpaceGameExceptions.h"
#include <string>
#include <algorithm>

void Game::startGame(){
    bool valid=false;
    while (!valid) {
        std::cout << "\nNewgame or Load:";
        std::string selection="";
        std::cin >> selection;
        std::transform(selection.begin(), selection.end(),selection.begin(), ::toupper);
        if (selection=="NEWGAME") {
            valid=true;
            newGame();
        }
        else if (selection=="LOAD") {
            valid=true;
            load();
        }
    }
}

void Game::getUserInput(std::istream &in, std::ostream &out){
    out << "\n:";
    std::string action;
    in >> action;
    currentRun.push_back(action);
    std::transform(action.begin(), action.end(),action.begin(), ::toupper);
    //Error handling for improper player input in this block
    try
    {
        if(action=="WALK"){
            in >>action;
            currentRun.push_back(action);
            player->walkTo(action);
            //  player->walkTo(action); Moved up so that an error can be thrown before saving the move.
            //out << player->isIn->getDiscription();
        }
        else if (action=="HELP")
            help();
        else if (action=="INSPECT"){
            in >>action;
            currentRun.push_back(action);
            if(player->inspect(action)=="")
            {
                //If an empty string is returned, then a bad parameter was entered
                //warranting the exception. throwing here also prevents bad input from
                //being saved in the save file.
                throw invalid_inspection_parameter();
            }
            out << player->inspect(action);
        }
        else if (action=="SAVE"){
            currentRun.pop_back();
            save();
        }
        else if (action=="QUIT"){
            currentRun.pop_back();
            currentRun.push_back("QUIT");
        }
        else if(action=="TALK")
        {
            in>>action;
            currentRun.push_back(action);
            if(player->talk(action)=="")
            {
                throw invalid_talk_error();
            }
            out << player->talk(action);
        }
         else if(action=="POKE")
        {
            in>>action;
            currentRun.push_back(action);
            if(player->poke(action)=="")
            {
                throw invalid_poke_error();
            }
            out << player->poke(action);
        }
        else if(action == "PICKUP")
        {
            in>>action;
            std::transform(action.begin(), action.end(),action.begin(), ::toupper);
            currentRun.push_back(action);
            player->pickup(action);
            std::cout<<"You picked up: "<<action<<std::endl;
        }
       /* else if(action =="ACTIVATE")
        {
            in>>action;
            std::transform(action.begin(), action.end(),action.begin(), ::toupper);
            currentRun.push_back(action);
            player->activate(action);
        }*/

        //This else should only run if a command entered did not match any known command. Add any commands above this line.
        else
        {
            throw invalid_command_error();
        }
    } //end try
    
    //Begin user input catch statements
    catch(invalid_inspection_parameter& p)
    {
        std::cout<<"Error: "<<p.what()<<" Try inspecting something else."<<std::endl;
    } //end inspection error handling
    catch(invalid_walk_input_error& p)
    {
        if(player->isIn->getName()=="Elevator")
            std::cout<<"Error: "<<p.what()<<" Try one of the three floor select commands: Top, Middle, Bottom."<<std::endl;
        else
            std::cout<<"Error: "<<p.what()<<" Try one of the four direction commands: Aft, Port, Starboard, or Bow."<<std::endl;
    }//end invalid input parameter for walking input error handling
    catch(out_of_bounds_error& p)
    {
        std::cout<<"Error: "<<p.what()<<" There is no exit in this direction, try another."<<std::endl;
    }//end walking out of bounds error handling
    catch(invalid_command_error& p){
        std::cout<<"Error: "<<p.what()<<" Try help to get a list of available commands"<<std::endl;
    }
    catch(invalid_talk_error& p)
    {
        std::cout<<"Error: "<<p.what()<<" Try using the name of the NPC in the room"<<std::endl;
    }
     catch(invalid_poke_error& p)
    {
        std::cout<<"Error: "<<p.what()<<" Try using the name of the NPC in the room"<<std::endl;
    }
    catch(invalid_object_usage_error& p)
    {
        std::cout<<"Error: "<<p.what()<<" Check the name of the input object and make sure it is in the room."<<std::endl;
    }
};

Game::Game(){
    player=new Player(nullptr);
}
