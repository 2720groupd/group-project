//
//  Ship.h
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#ifndef Ship_h
#define Ship_h
#include "Room.h"
#include <vector>
#include "NPC.h"
#include "Object.h"
#include "Player.h"

    /**
    *Places everything in the ship, as well as creates the rooms and places npcs
    */
class Ship{
public:
    Ship(Player*);
    ~Ship();
    Room* getHelm();
private:
    Room *helm;
        /**
    * all the rooms on the ship 
    */
    std::vector<Room*> allRooms;
    /**
    * all the npcs that are located around the ship 
    */
    std::vector<NPC*> allNPCs;
    /**
    * all possible objects the player will run into on the ship
    */
    std::vector<Object*> allObjects;

};

#endif /* Ship_h */
