//
//  Player.h
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#ifndef Player_h
#define Player_h

#include "Room.h"
#include "Object.h"
#include<vector>


    /**
    *class responsible for creating the player. Contains all the actions the player can do
    */
class Player{
public:
    Room* isIn;
    Player(Room*);
        /**
    *walks in the direction specified
    */
    void walkTo(std::string);
   /**
    *Puts the item in the specified room
    */
    void putInRoom(Room*);
     /**
    *inspects the specified item
    */
    std::string inspect(std::string);    
    /**
    *checks the items currently being held
    */
    std::string checkInventory() const;
    /**
    *talks to the specified npc
    */
    std::string talk(std::string) const;
    /**
    *pokes the specified npc
    */
    std::string poke(std::string) const;
  /**
    *Specfies the item to be repaired
    */
    void repair(std::string);
     /**
    *specifies the item to be activated
    */
    void activate(std::string);
     /**
    *picks up the specified item
    */
    void pickup(std::string);
  /**
    *returns vector of full player inventory
    */
    std::vector<Object*> getInventory();
      /**
    *Kill the player
    */
    void killPlayer();

 private:
    std::vector<Object*> playerInventory;
    bool playerAlive;
};

#endif /* Player_h */
