//
//  Room.h
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#ifndef Room_h
#define Room_h

#include <string>
#include <vector>
#include "Object.h"
#include "NPC.h"

     /**
    *creates the room, as well as what item goes into the room, and the npc
    */
class Room{
private:
    std::string name;
    bool roomLocked;
    std::string Discription;
    NPC *roomCharacter;
    Room* bow;
    Room* aft;
    Room* port;
    Room* starboard;
public:
    Room(std::string,std::string,Room*,Room*,Room*,Room* ,NPC*);
    ~Room();
    std::vector<Object*> roomObjects;
        /**
    *Sets the Bow Room
    */
    void setBowRoom(Room*);
    /**
    *Sets the Aft Room
    */
    void setAftRoom(Room*);
  /**
    *Sets the port room
    */
    void setPortRoom(Room*);
      /**
    *sets the Starboard room
    */
    void setStarboardRoom(Room*);
    /**
    *returns the bow room
    */
    Room* getBowRoom();
    /**
    *returns the aft room
    */
    Room* getAftRoom();
   /**
    *returns the port room
    */
    Room* getPortRoom();
     /**
    *returns the starboard room
    */
    Room* getStarboardRoom();

    std::string getName();
    void lock();
    void unlock();
    bool islocked();
    std::string getDiscription();
    /**
     *Sets the NPC in the room
     */
    void setNPC(NPC*);
        /**
    *Returns pointer of NPC
    */
    NPC* getNPC();
    /**
    *returns the specified object pointer
    */
    Object* getItem(std::string);

};

#endif /* Room_h */
