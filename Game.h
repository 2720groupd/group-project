//
//  Game.h
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#ifndef Game_h
#define Game_h

#include <iostream>
#include <vector>
#include "Player.h"


class Game{
public:
    Game();
    void startGame();
    void getUserInput(std::istream &in,std::ostream &out);
    virtual void play() =0;
    virtual void load() =0;
    virtual void save() =0;
    virtual void help() =0;
    virtual void newGame()=0;
    std::vector<std::string> currentRun;
    Player* player;
};

#endif /* Game_h */
