//**********************************************************
//  NPC.h
//  Group d Project
//  File contains definitions for NPC class
//
//  
//**********************************************************

#ifndef NPC_h
#define NPC_h

#include<iostream>
#include<vector>
#include<string>
#include<fstream>


    /**
    *class that creates the NPC
    */
class NPC
{
 public:
  ///Constructor that takes in the npc's name, two text file names containing dialogue, and a character description
  ///the text files are used to populate the dialogue vectors, a bool for the living status of the NPC is also included, and defaults to true.
  NPC(std::string charInfoFile, std::string regDialogue, std::string annoyedDialogue);
  ~NPC();
  std::string respond() const;
      /**
    *returns the annoyes respond by the npc
    */
  std::string respondAnnoyed();
    /**
    *returns the description of the NPC
    */
  std::string getDescription() const;
    /**
    *returns the alive status of npc
    */
  bool getStatus() const;

  std::string getName() const;
 std::vector<std::string> getDialogue() const;
 std::vector<std::string> getAnnoyedDialogue() const;
  int timesAnnoyed=0;
  void die();
  //test function ->remove later
  void testDialogue(std::vector<std::string> dialogue) const;
 private:
  ///the bool isAlive is used to distinguish living NPC's from dead ones, the description of an NPC changes based on its status
  bool isAlive;
  std::vector<std::string> regularDialogue;
  std::vector<std::string> annoyedDialogue;
  std::string aliveDescription;
  std::string deadDescription;
  std::string npcName;
  
  
  
};

#endif /* NPC_h */
