//**********************************************************
//  Pickup.h
//  Group d Project
//  File contains definitions for Pickup class, concrete class of Object
//
//  
//**********************************************************

#ifndef Pickup_h
#define Pickup_h

#include<vector>
#include<iostream>
#include<string>
#include<fstream>
#include "Object.h"

    /**
    *Implementation of object. Pick ups are items that can only be picked up. 
    */

class Pickup : public Object
{
 public:
  ///Pickup constructor sets name and description for item, as well as sets the items used status to false
  Pickup(std::string infoFile, bool hidden);
  ~Pickup();
    ///sets items used status to true

  bool getUsedStatus();
  void setUsed();
  
 private:
  bool isUsed;
};


#endif /* Pickup_h */
