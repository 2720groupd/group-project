#ifndef UtilityFunctions_h
#define UtilityFunctions_h

#include<string>
#include<vector>
namespace utility
{
  ///@brief This method takes in a filename and copies all text from the file to a vector of strings and returns it.
  ///@param string fileName: name or directory path of file containing text
  ///@return vector<string> containing the lines of the text file
  std::vector<std::string> readInFileString(std::string fileName);
  
}

#endif
