//**********************************************************
//  Object.cpp
//  Group d Project
//  File contains implementation for Object abstract class
//
//  
//**********************************************************

#include "Object.h"
#include "UtilityFunctions.h"

Object::Object(std::string infoFile, bool hidden) : hidden(hidden)
{
std::vector<std::string> infoTempParse = utility::readInFileString(infoFile);
objectName = infoTempParse[0];
objectDescription = infoTempParse[1];
}


Object::~Object(){}

std::string Object::getName() const
{
  return objectName;  
}

std::string Object::getDescription() const
{
  return objectDescription;
}

void Object::setName(std::string name)
{
  objectName = name;
}

void Object::setDescription(std::string description)
{
  objectDescription = description;
}

void Object::revealObject()
{
	hidden = false;
}

bool Object::isHidden()
{
	return hidden;
}