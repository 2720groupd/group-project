//
//  SpaceAdventureGame.h
//  Group Project
//
//  Created by Andrew Chichak on 2015-10-28.
//  Copyright © 2015 Andrew Chichak. All rights reserved.
//

#ifndef SpaceAdventureGame_h
#define SpaceAdventureGame_h

#include <iostream>
#include <vector>
#include "Game.h"
#include "Player.h"
#include "Ship.h"

///implementation of the game class
class SpaceAdventureGame: public Game{
public:
        /**
    *Sets the game up
    */
    void play();
    
    
    /**
     *The dialog sections of the game
     *with the string being the file to use and the int being the location of the escape parameter and the number of lines the intro is
     */
    void dialog(std::string,int,int);
    
    /**
    *Opens save.txt and checks to see how far the player is in the game
    */
    void load();
  /**
    *Writes out all of the moves that have happened to save.txt
    */
    void save();
      /**
    *Reads out all the possible commands for the game
    */
    void help();
   /**
    *Starts the game from the beginning
    */
    void newGame();
 
    SpaceAdventureGame();
private:
    bool phase0;
    bool phase2;
    bool phase6;
    bool phase8;

    Ship gameShip;
};

#endif /* SpaceAdventureGame_h */
